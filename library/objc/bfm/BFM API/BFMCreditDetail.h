#import <Foundation/Foundation.h>
#import "BFMObject.h"

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

#import "BFMPromiscuousTimeline.h"
#import "BFMLoan.h"
#import "BFMAdvanceTimeline.h"
#import "BFMCashflow.h"


@protocol BFMCreditDetail
@end

@interface BFMCreditDetail : BFMObject


@property(nonatomic) NSString* _id;

@property(nonatomic) NSString* type;

@property(nonatomic) NSString* code;

@property(nonatomic) NSString* associatedAccount;

@property(nonatomic) NSNumber* creditGranted;

@property(nonatomic) NSNumber* creditAvailable;

@property(nonatomic) NSString* currency;

@property(nonatomic) NSDate* createDate;

@property(nonatomic) BFMCashflow* cashflowDetails;

@property(nonatomic) BFMAdvanceTimeline* advanceDetails;

@property(nonatomic) BFMPromiscuousTimeline* promiscuousDetails;

@property(nonatomic) BFMLoan* loanDetails;

@end
