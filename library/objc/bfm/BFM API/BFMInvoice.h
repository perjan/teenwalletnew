#import <Foundation/Foundation.h>
#import "BFMObject.h"

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */



@protocol BFMInvoice
@end

@interface BFMInvoice : BFMObject


@property(nonatomic) NSString* number;

@property(nonatomic) NSString* vAT;

@property(nonatomic) NSString* debtorName;

@property(nonatomic) NSNumber* advanceAmount;

@property(nonatomic) NSNumber* amount;

@property(nonatomic) NSDate* issueDate;

@property(nonatomic) NSDate* expirationDate;

@end
