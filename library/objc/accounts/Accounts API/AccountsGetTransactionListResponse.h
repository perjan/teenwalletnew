#import <Foundation/Foundation.h>
#import "AccountsObject.h"

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

#import "AccountsTransactionItem.h"
#import "AccountsHeader.h"


@protocol AccountsGetTransactionListResponse
@end

@interface AccountsGetTransactionListResponse : AccountsObject


@property(nonatomic) AccountsHeader* transactionHeader;

@property(nonatomic) NSArray<AccountsTransactionItem>* transactions;

@end
