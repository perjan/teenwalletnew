#import "AccountsApi.h"
#import "AccountsQueryParamCollection.h"
#import "AccountsGetAccountsResponse.h"
#import "AccountsGetAccountResponse.h"
#import "AccountsPlafond.h"
#import "AccountsGetTransactionListResponse.h"


@interface AccountsApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation AccountsApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        AccountsConfiguration *config = [AccountsConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[AccountsApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(AccountsApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(AccountsApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static AccountsApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[AccountsApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [AccountsApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Return summary of user accounts
/// 
///  @param updatedTo To update date
///
///  @param ownerName Account Owner Name
///
///  @param updatedFrom From update date filter
///
///  @param status Account status
///
///  @param userId User unique identfier
///
///  @returns AccountsGetAccountsResponse*
///
-(NSNumber*) getAccountsWithCompletionBlock: (NSDate*) updatedTo
         ownerName: (NSString*) ownerName
         updatedFrom: (NSDate*) updatedFrom
         status: (NSString*) status
         userId: (NSString*) userId
        
        completionHandler: (void (^)(AccountsGetAccountsResponse* output, NSError* error))completionBlock { 
        

    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/accounts/v1"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(updatedTo != nil) {
        
        queryParams[@"updatedTo"] = updatedTo;
    }
    if(ownerName != nil) {
        
        queryParams[@"ownerName"] = ownerName;
    }
    if(updatedFrom != nil) {
        
        queryParams[@"updatedFrom"] = updatedFrom;
    }
    if(status != nil) {
        
        queryParams[@"status"] = status;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [AccountsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [AccountsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"AccountsGetAccountsResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((AccountsGetAccountsResponse*)data, error);
              }
          ];
}

///
/// Return informations about a specific user account
/// 
///  @param accountId Identifier for the Account
///
///  @param userId User unique identifier
///
///  @returns AccountsGetAccountResponse*
///
-(NSNumber*) getAccountDetailWithCompletionBlock: (NSString*) accountId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(AccountsGetAccountResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'accountId' is set
    if (accountId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `accountId` when calling `getAccountDetail`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/accounts/v1/{accountId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (accountId != nil) {
        pathParams[@"accountId"] = accountId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [AccountsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [AccountsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"AccountsGetAccountResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((AccountsGetAccountResponse*)data, error);
              }
          ];
}

///
/// Return Plafond Information
/// 
///  @param accountId Identifier for the Account
///
///  @param userId User unique identifier
///
///  @returns AccountsPlafond*
///
-(NSNumber*) getAccountPlafondWithCompletionBlock: (NSString*) accountId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(AccountsPlafond* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'accountId' is set
    if (accountId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `accountId` when calling `getAccountPlafond`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/accounts/v1/{accountId}/plafond"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (accountId != nil) {
        pathParams[@"accountId"] = accountId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [AccountsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [AccountsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"AccountsPlafond*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((AccountsPlafond*)data, error);
              }
          ];
}

///
/// Returns information about account transactions 
/// 
///  @param accountId Identifier for the Account
///
///  @param type Get the transaction of the selelted type
///
///  @param startDate Get the transaction after this date
///
///  @param endDate Get the transactions before this date
///
///  @param userId User unique identifier
///
///  @returns AccountsGetTransactionListResponse*
///
-(NSNumber*) getAccountTransactionsWithCompletionBlock: (NSString*) accountId
         type: (NSString*) type
         startDate: (NSDate*) startDate
         endDate: (NSDate*) endDate
         userId: (NSString*) userId
        
        completionHandler: (void (^)(AccountsGetTransactionListResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'accountId' is set
    if (accountId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `accountId` when calling `getAccountTransactions`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/accounts/v1/{accountId}/transactions"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (accountId != nil) {
        pathParams[@"accountId"] = accountId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(type != nil) {
        
        queryParams[@"type"] = type;
    }
    if(startDate != nil) {
        
        queryParams[@"startDate"] = startDate;
    }
    if(endDate != nil) {
        
        queryParams[@"endDate"] = endDate;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [AccountsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [AccountsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"AccountsGetTransactionListResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((AccountsGetTransactionListResponse*)data, error);
              }
          ];
}



@end
