#import <Foundation/Foundation.h>
#import "AccountsObject.h"

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

#import "AccountsOwnerSummary.h"
#import "AccountsBankSummary.h"


@protocol AccountsAccountSummary
@end

@interface AccountsAccountSummary : AccountsObject


@property(nonatomic) NSString* _id;

@property(nonatomic) NSString* number;

@property(nonatomic) AccountsOwnerSummary* owner;

@property(nonatomic) AccountsBankSummary* bank;

@property(nonatomic) NSString* iBAN;

@end
