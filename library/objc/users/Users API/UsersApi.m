#import "UsersApi.h"
#import "UsersQueryParamCollection.h"
#import "UsersGetUserInfoResponse.h"
#import "UsersPutUserInfoResponse.h"
#import "UsersPutUserInfoRequest.h"


@interface UsersApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation UsersApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        UsersConfiguration *config = [UsersConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[UsersApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(UsersApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(UsersApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static UsersApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[UsersApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [UsersApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Retrieve User Info
/// 
///  @param userId User Identifier
///
///  @returns UsersGetUserInfoResponse*
///
-(NSNumber*) getUserInfoWithCompletionBlock: (NSString*) userId
        
        completionHandler: (void (^)(UsersGetUserInfoResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'userId' is set
    if (userId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `userId` when calling `getUserInfo`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/users/v1/current/{userId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (userId != nil) {
        pathParams[@"userId"] = userId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [UsersApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [UsersApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"UsersGetUserInfoResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((UsersGetUserInfoResponse*)data, error);
              }
          ];
}

///
/// Update User Info
/// 
///  @param userId User Identifier
///
///  @param newInfo Info to be updated
///
///  @returns UsersPutUserInfoResponse*
///
-(NSNumber*) updateUserInfoWithCompletionBlock: (NSString*) userId
         newInfo: (UsersPutUserInfoRequest*) newInfo
        
        completionHandler: (void (^)(UsersPutUserInfoResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'userId' is set
    if (userId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `userId` when calling `updateUserInfo`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/users/v1/current/{userId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (userId != nil) {
        pathParams[@"userId"] = userId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [UsersApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [UsersApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = newInfo;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"PUT"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"UsersPutUserInfoResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((UsersPutUserInfoResponse*)data, error);
              }
          ];
}



@end
