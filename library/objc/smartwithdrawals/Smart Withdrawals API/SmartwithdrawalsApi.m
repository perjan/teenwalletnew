#import "SmartwithdrawalsApi.h"
#import "SWQueryParamCollection.h"
//#import "SWWithdrawals.h"
#import "SWCreateWithdrawal.h"
#import "SWWithdrawal.h"
#import "SWUpdateWithdrawal.h"
#import "SWWithdrawalStatus.h"
#import "SWSign.h"
#import "SWWithdrawalSign.h"


@interface SmartwithdrawalsApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation SmartwithdrawalsApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        SWConfiguration *config = [SWConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[SWApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(SWApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(SmartwithdrawalsApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static SmartwithdrawalsApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[SmartwithdrawalsApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [SWApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Gets information about all withdrawal reservations
/// 
///  @param bookingDateFrom selects the withdrawals occurred after the booking date specified
///
///  @param executeDateFrom selects the withdrawals occurred after the execute date specified
///
///  @param statusCode selects the withdrawals with the status specified
///
///  @param executeDateTo selects the withdrawals occurred before the execute date specified
///
///  @param amountLT selects the withdrawals with amount smaller than the specified value
///
///  @param accountNumber selects the withdrawals with the account number specified
///
///  @param bookingDateTo selects the withdrawals occurred before the booking date specified
///
///  @param amountGT selects the withdrawals with amount greater than the specified value
///
///  @param userId User unique identifier
///
///  @returns SWWithdrawals*
///
-(NSNumber*) getSmartWithdrawalsWithCompletionBlock: (NSDate*) bookingDateFrom
         executeDateFrom: (NSDate*) executeDateFrom
         statusCode: (NSString*) statusCode
         executeDateTo: (NSDate*) executeDateTo
         amountLT: (NSNumber*) amountLT
         accountNumber: (NSString*) accountNumber
         bookingDateTo: (NSDate*) bookingDateTo
         amountGT: (NSNumber*) amountGT
         userId: (NSString*) userId
        
        completionHandler: (void (^)(SWWithdrawal* output, NSError* error))completionBlock { 
        

    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/smartwithdrawals/v1"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(bookingDateFrom != nil) {
        
        queryParams[@"bookingDateFrom"] = bookingDateFrom;
    }
    if(executeDateFrom != nil) {
        
        queryParams[@"executeDateFrom"] = executeDateFrom;
    }
    if(statusCode != nil) {
        
        queryParams[@"statusCode"] = statusCode;
    }
    if(executeDateTo != nil) {
        
        queryParams[@"executeDateTo"] = executeDateTo;
    }
    if(amountLT != nil) {
        
        queryParams[@"amountLT"] = amountLT;
    }
    if(accountNumber != nil) {
        
        queryParams[@"accountNumber"] = accountNumber;
    }
    if(bookingDateTo != nil) {
        
        queryParams[@"bookingDateTo"] = bookingDateTo;
    }
    if(amountGT != nil) {
        
        queryParams[@"amountGT"] = amountGT;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [SWApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"SWWithdrawals*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((SWWithdrawal*)data, error);
              }
          ];
}

///
/// Creates a withdrawal reservation
/// 
///  @param body smartWithdrawal information
///
///  @param userId User unique identifier
///
///  @returns SWWithdrawal*
///
-(NSNumber*) createSmartWithdrawalWithCompletionBlock: (SWCreateWithdrawal*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(SWWithdrawal* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `createSmartWithdrawal`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/smartwithdrawals/v1"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [SWApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"POST"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"SWWithdrawal*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((SWWithdrawal*)data, error);
              }
          ];
}

///
/// Gets information about specific withdrawal reservations
/// 
///  @param reservationId id of withdrawal reservation
///
///  @param userId User unique identifier
///
///  @returns SWWithdrawal*
///
-(NSNumber*) getSmartWithdrawalWithCompletionBlock: (NSString*) reservationId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(SWWithdrawal* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'reservationId' is set
    if (reservationId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `reservationId` when calling `getSmartWithdrawal`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/smartwithdrawals/v1/{reservationId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (reservationId != nil) {
        pathParams[@"reservationId"] = reservationId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [SWApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"SWWithdrawal*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((SWWithdrawal*)data, error);
              }
          ];
}

///
/// Updates a specific withdrawal reservation
/// 
///  @param reservationId id of withdrawal reservation
///
///  @param body smartWithdrawal information
///
///  @param userId User unique identifier
///
///  @returns SWWithdrawals*
///
-(NSNumber*) updateSmartWithdrawalWithCompletionBlock: (NSString*) reservationId
         body: (SWUpdateWithdrawal*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(SWWithdrawal* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'reservationId' is set
    if (reservationId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `reservationId` when calling `updateSmartWithdrawal`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `updateSmartWithdrawal`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/smartwithdrawals/v1/{reservationId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (reservationId != nil) {
        pathParams[@"reservationId"] = reservationId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [SWApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"PUT"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"SWWithdrawal*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((SWWithdrawal*)data, error);
              }
          ];
}

///
/// Deletes a specific withdrawal reservation
/// 
///  @param reservationId id of withdrawal reservation
///
///  @param userId User unique identifier
///
///  @returns SWWithdrawalStatus*
///
-(NSNumber*) deleteSmartWithdrawalWithCompletionBlock: (NSString*) reservationId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(SWWithdrawalStatus* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'reservationId' is set
    if (reservationId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `reservationId` when calling `deleteSmartWithdrawal`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/smartwithdrawals/v1/{reservationId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (reservationId != nil) {
        pathParams[@"reservationId"] = reservationId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [SWApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"DELETE"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"SWWithdrawalStatus*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((SWWithdrawalStatus*)data, error);
              }
          ];
}

///
/// Returns the information to perform the sign operation
/// 
///  @param reservationId id of withdrawal reservation
///
///  @param userId User unique identifier
///
///  @returns SWSign*
///
-(NSNumber*) getSmartWithdrawalSignWithCompletionBlock: (NSString*) reservationId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(SWSign* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'reservationId' is set
    if (reservationId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `reservationId` when calling `getSmartWithdrawalSign`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/smartwithdrawals/v1/{reservationId}/sign"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (reservationId != nil) {
        pathParams[@"reservationId"] = reservationId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [SWApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"SWSign*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((SWSign*)data, error);
              }
          ];
}

///
/// Executes the sign operation and performs withdrawal
/// 
///  @param reservationId id of withdrawal reservation
///
///  @param body smartWithdrawal information
///
///  @param userId User unique identifier
///
///  @returns SWWithdrawalStatus*
///
-(NSNumber*) executeSmartWithdrawalSignWithCompletionBlock: (NSString*) reservationId
         body: (SWWithdrawalSign*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(SWWithdrawalStatus* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'reservationId' is set
    if (reservationId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `reservationId` when calling `executeSmartWithdrawalSign`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `executeSmartWithdrawalSign`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/smartwithdrawals/v1/{reservationId}/sign"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (reservationId != nil) {
        pathParams[@"reservationId"] = reservationId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [SWApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"POST"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"SWWithdrawalStatus*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((SWWithdrawalStatus*)data, error);
              }
          ];
}



@end
