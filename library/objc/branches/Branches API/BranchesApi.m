#import "BranchesApi.h"
#import "BranchesQueryParamCollection.h"
#import "BranchesGetBranchesResponse.h"
#import "BranchesGetBranchResponse.h"


@interface BranchesApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation BranchesApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        BranchesConfiguration *config = [BranchesConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[BranchesApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(BranchesApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(BranchesApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static BranchesApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[BranchesApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [BranchesApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Retrieve Branch Information
/// 
///  @param x longitude coordinate of the ATM
///
///  @param y latitude coordinate of the ATM
///
///  @param distance distance within which to seek ATMs from the coordinates specified
///
///  @param maxResult maximum number of results to include in the results
///
///  @param userId User unique identifier
///
///  @returns BranchesGetBranchesResponse*
///
-(NSNumber*) getBranchesWithCompletionBlock: (NSNumber*) x
         y: (NSNumber*) y
         distance: (NSNumber*) distance
         maxResult: (NSNumber*) maxResult
         userId: (NSString*) userId
        
        completionHandler: (void (^)(BranchesGetBranchesResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'x' is set
    if (x == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `x` when calling `getBranches`"];
    }
    
    // verify the required parameter 'y' is set
    if (y == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `y` when calling `getBranches`"];
    }
    
    // verify the required parameter 'distance' is set
    if (distance == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `distance` when calling `getBranches`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bank/v1/branches"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(x != nil) {
        
        queryParams[@"x"] = x;
    }
    if(y != nil) {
        
        queryParams[@"y"] = y;
    }
    if(distance != nil) {
        
        queryParams[@"distance"] = distance;
    }
    if(maxResult != nil) {
        
        queryParams[@"maxResult"] = maxResult;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BranchesApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BranchesApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BranchesGetBranchesResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BranchesGetBranchesResponse*)data, error);
              }
          ];
}

///
/// Returns information about a specific branch
/// 
///  @param _id Unique identifier for the branch
///
///  @param userId User unique identifier
///
///  @returns BranchesGetBranchResponse*
///
-(NSNumber*) getBranchWithCompletionBlock: (NSString*) _id
         userId: (NSString*) userId
        
        completionHandler: (void (^)(BranchesGetBranchResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter '_id' is set
    if (_id == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `_id` when calling `getBranch`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bank/v1/branches/{id}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BranchesApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BranchesApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BranchesGetBranchResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BranchesGetBranchResponse*)data, error);
              }
          ];
}



@end
