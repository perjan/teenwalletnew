#import "PaymentsApi.h"
#import "PaymentsQueryParamCollection.h"
#import "PaymentsReadDomesticTransferArray.h"
#import "PaymentsCreateDomesticTransferRequest.h"
#import "PaymentsDomesticTransferResponse.h"
#import "PaymentsDomesticTransferDetail.h"
#import "PaymentsUpdateDomesticTransferRequest.h"
#import "PaymentsUpdateDomesticTransferResponse.h"
#import "PaymentsDomesticTransferStatus.h"
#import "PaymentsSignDomesticTransfer.h"
#import "PaymentsSubscribeDomesticTransfer.h"
#import "PaymentsSubscribeDomesticPaymentResponse.h"


@interface PaymentsApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation PaymentsApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        PaymentsConfiguration *config = [PaymentsConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[PaymentsApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(PaymentsApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(PaymentsApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static PaymentsApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[PaymentsApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [PaymentsApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Get all Domestic Payments, filter by Sender Account ID, Sender Account Type, Payment Issue Date (from – to), Payment Flag Immediate, Lifecycle Status Code / Message, Recipient Name
/// 
///  @param settlementDateFrom filter the result whose settlementDate is before the one specified
///
///  @param issueDateTo filter the result whose issueDate is after the one specified
///
///  @param amountGT filter the result whose amount is less than the one specified
///
///  @param draft if \"true\" is DRAFT
///
///  @param issueDateFrom filter the result whose issueDate is before the one specified
///
///  @param settlementDateTo filter the result whose settlementDate is after the one specified
///
///  @param beneficiaryName filter the result by recipent name
///
///  @param amountLT filter the result whose amount is greater than the one specified
///
///  @param statusCode filter the result by lifecycle status code
///
///  @param immediate filter the result by immediate flag
///
///  @param accountNumber filter by account Number
///
///  @param userId Unique user identifier
///
///  @returns PaymentsReadDomesticTransferArray*
///
-(NSNumber*) getAllDomesticPaymentsWithCompletionBlock: (NSDate*) settlementDateFrom
         issueDateTo: (NSDate*) issueDateTo
         amountGT: (NSNumber*) amountGT
         draft: (NSString*) draft
         issueDateFrom: (NSDate*) issueDateFrom
         settlementDateTo: (NSDate*) settlementDateTo
         beneficiaryName: (NSString*) beneficiaryName
         amountLT: (NSNumber*) amountLT
         statusCode: (NSString*) statusCode
         immediate: (NSString*) immediate
         accountNumber: (NSString*) accountNumber
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsReadDomesticTransferArray* output, NSError* error))completionBlock { 
        

    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(settlementDateFrom != nil) {
        
        queryParams[@"settlementDateFrom"] = settlementDateFrom;
    }
    if(issueDateTo != nil) {
        
        queryParams[@"issueDateTo"] = issueDateTo;
    }
    if(amountGT != nil) {
        
        queryParams[@"amountGT"] = amountGT;
    }
    if(draft != nil) {
        
        queryParams[@"draft"] = draft;
    }
    if(issueDateFrom != nil) {
        
        queryParams[@"issueDateFrom"] = issueDateFrom;
    }
    if(settlementDateTo != nil) {
        
        queryParams[@"settlementDateTo"] = settlementDateTo;
    }
    if(beneficiaryName != nil) {
        
        queryParams[@"beneficiaryName"] = beneficiaryName;
    }
    if(amountLT != nil) {
        
        queryParams[@"amountLT"] = amountLT;
    }
    if(statusCode != nil) {
        
        queryParams[@"statusCode"] = statusCode;
    }
    if(immediate != nil) {
        
        queryParams[@"immediate"] = immediate;
    }
    if(accountNumber != nil) {
        
        queryParams[@"accountNumber"] = accountNumber;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsReadDomesticTransferArray*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsReadDomesticTransferArray*)data, error);
              }
          ];
}

///
/// Create a Domestic Payment
/// 
///  @param body Domestic payment information
///
///  @param userId Unique user identifier
///
///  @returns PaymentsDomesticTransferResponse*
///
-(NSNumber*) createDomesticPaymentWithCompletionBlock: (PaymentsCreateDomesticTransferRequest*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsDomesticTransferResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `createDomesticPayment`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"POST"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsDomesticTransferResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsDomesticTransferResponse*)data, error);
              }
          ];
}

///
/// Retrieve a single domestic payment
/// 
///  @param tID Domestic Payment TID
///
///  @param draft if \"true\" is DRAFT
///
///  @param userId Unique user identifier
///
///  @returns PaymentsDomesticTransferDetail*
///
-(NSNumber*) getDomesticPaymentWithCompletionBlock: (NSString*) tID
         draft: (NSString*) draft
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsDomesticTransferDetail* output, NSError* error))completionBlock { 
        
    // verify the required parameter 'tID' is set
    if (tID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `tID` when calling `getDomesticPayment`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers/{TID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tID != nil) {
        pathParams[@"TID"] = tID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(draft != nil) {
        
        queryParams[@"draft"] = draft;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsDomesticTransferDetail*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsDomesticTransferDetail*)data, error);
              }
          ];
}

///
/// Update an existing Payment given its ID
/// 
///  @param tID Domestic Payment TID
///
///  @param body New payment information
///
///  @param userId Unique user identifier
///
///  @returns PaymentsUpdateDomesticTransferResponse*
///
-(NSNumber*) updateDomesticPaymentWithCompletionBlock: (NSString*) tID
         body: (PaymentsUpdateDomesticTransferRequest*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsUpdateDomesticTransferResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'tID' is set
    if (tID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `tID` when calling `updateDomesticPayment`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `updateDomesticPayment`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers/{TID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tID != nil) {
        pathParams[@"TID"] = tID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"PUT"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsUpdateDomesticTransferResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsUpdateDomesticTransferResponse*)data, error);
              }
          ];
}

///
/// Delete an existing payment given its TID
/// 
///  @param tID Domestic Payments TID
///
///  @param userId Unique user identifier
///
///  @returns PaymentsDomesticTransferStatus*
///
-(NSNumber*) deletePaymentWithCompletionBlock: (NSString*) tID
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsDomesticTransferStatus* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'tID' is set
    if (tID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `tID` when calling `deletePayment`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers/{TID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tID != nil) {
        pathParams[@"TID"] = tID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"DELETE"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsDomesticTransferStatus*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsDomesticTransferStatus*)data, error);
              }
          ];
}

///
/// GET Payments Sign
/// 
///  @param tID Domestic Transfer TID
///
///  @param userId Unique user identifier
///
///  @returns PaymentsSignDomesticTransfer*
///
-(NSNumber*) getPaymentsSignWithCompletionBlock: (NSString*) tID
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsSignDomesticTransfer* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'tID' is set
    if (tID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `tID` when calling `getPaymentsSign`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers/{TID}/sign"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tID != nil) {
        pathParams[@"TID"] = tID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsSignDomesticTransfer*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsSignDomesticTransfer*)data, error);
              }
          ];
}

///
/// Post Payment Sign
/// 
///  @param tID Domestic Payment TID
///
///  @param body Object to sign the payment
///
///  @param userId Unique user identifier
///
///  @returns PaymentsDomesticTransferStatus*
///
-(NSNumber*) postPaymentSignWithCompletionBlock: (NSString*) tID
         body: (PaymentsSignDomesticTransfer*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsDomesticTransferStatus* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'tID' is set
    if (tID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `tID` when calling `postPaymentSign`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `postPaymentSign`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers/{TID}/sign"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tID != nil) {
        pathParams[@"TID"] = tID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"POST"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsDomesticTransferStatus*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsDomesticTransferStatus*)data, error);
              }
          ];
}

///
/// Post Payment Subscribe
/// 
///  @param tID Domestic Payment TID
///
///  @param body Object to subscribe the payment
///
///  @param userId Unique user identifier
///
///  @returns PaymentsSubscribeDomesticPaymentResponse*
///
-(NSNumber*) postPaymentSubscribeWithCompletionBlock: (NSString*) tID
         body: (PaymentsSubscribeDomesticTransfer*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(PaymentsSubscribeDomesticPaymentResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'tID' is set
    if (tID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `tID` when calling `postPaymentSubscribe`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `postPaymentSubscribe`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/v1/domesticTransfers/{TID}/subscribe"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tID != nil) {
        pathParams[@"TID"] = tID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [PaymentsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [PaymentsApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"POST"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"PaymentsSubscribeDomesticPaymentResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((PaymentsSubscribeDomesticPaymentResponse*)data, error);
              }
          ];
}



@end
