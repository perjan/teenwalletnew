#import "TransactionsListApi.h"
#import "TLQueryParamCollection.h"
#import "TLGetTransactionListResponse.h"
#import "TLGetTransactionResponse.h"
#import "TLPutMetaRequest.h"
#import "TLPutMetaResponse.h"
#import "TLPostMetaResponse.h"
#import "TLPostMetaRequest.h"


@interface TransactionsListApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation TransactionsListApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        TLConfiguration *config = [TLConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[TLApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(TLApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(TransactionsListApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static TransactionsListApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[TransactionsListApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [TLApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Returns meta information about user transactions
/// 
///  @param type Transaction Type
///
///  @param endDate Booking End Date
///
///  @param category Transaction Category
///
///  @param startDate Booking Start Date
///
///  @param userId User unique identifier
///
///  @returns TLGetTransactionListResponse*
///
-(NSNumber*) getUserTransactionsWithCompletionBlock: (NSString*) type
         endDate: (NSDate*) endDate
         category: (NSString*) category
         startDate: (NSDate*) startDate
         userId: (NSString*) userId
        
        completionHandler: (void (^)(TLGetTransactionListResponse* output, NSError* error))completionBlock { 
        

    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/transactions/v1"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(type != nil) {
        
        queryParams[@"type"] = type;
    }
    if(endDate != nil) {
        
        queryParams[@"endDate"] = endDate;
    }
    if(category != nil) {
        
        queryParams[@"category"] = category;
    }
    if(startDate != nil) {
        
        queryParams[@"startDate"] = startDate;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [TLApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [TLApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"TLGetTransactionListResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((TLGetTransactionListResponse*)data, error);
              }
          ];
}

///
/// Returns meta information about a specific transaction
/// 
///  @param transactionId Transaction Id
///
///  @param userId User unique identifier
///
///  @returns TLGetTransactionResponse*
///
-(NSNumber*) getTransactionWithCompletionBlock: (NSString*) transactionId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(TLGetTransactionResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'transactionId' is set
    if (transactionId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `transactionId` when calling `getTransaction`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/transactions/v1/{transactionId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (transactionId != nil) {
        pathParams[@"transactionId"] = transactionId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [TLApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [TLApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"TLGetTransactionResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((TLGetTransactionResponse*)data, error);
              }
          ];
}

///
/// Updates a meta data for a specific transaction
/// 
///  @param transactionId Transaction Id
///
///  @param meta Meta information
///
///  @param userId User unique identifier
///
///  @returns TLPutMetaResponse*
///
-(NSNumber*) updateMetaTransactionWithCompletionBlock: (NSString*) transactionId
         meta: (TLPutMetaRequest*) meta
         userId: (NSString*) userId
        
        completionHandler: (void (^)(TLPutMetaResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'transactionId' is set
    if (transactionId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `transactionId` when calling `updateMetaTransaction`"];
    }
    
    // verify the required parameter 'meta' is set
    if (meta == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `meta` when calling `updateMetaTransaction`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/transactions/v1/{transactionId}/meta"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (transactionId != nil) {
        pathParams[@"transactionId"] = transactionId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [TLApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [TLApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = meta;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"PUT"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"TLPutMetaResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((TLPutMetaResponse*)data, error);
              }
          ];
}

///
/// Creates a meta data for a specific transaction
/// 
///  @param transactionId Transaction Id
///
///  @param body Meta information object
///
///  @param userId User unique identifier
///
///  @returns TLPostMetaResponse*
///
-(NSNumber*) createMetaTransactionWithCompletionBlock: (NSString*) transactionId
         body: (TLPostMetaRequest*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(TLPostMetaResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'transactionId' is set
    if (transactionId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `transactionId` when calling `createMetaTransaction`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `createMetaTransaction`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/transactions/v1/{transactionId}/meta"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (transactionId != nil) {
        pathParams[@"transactionId"] = transactionId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [TLApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [TLApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"POST"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"TLPostMetaResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((TLPostMetaResponse*)data, error);
              }
          ];
}



@end
