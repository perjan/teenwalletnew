#import "ATMApi.h"
#import "ATMQueryParamCollection.h"
#import "ATMGetAtmsResponse.h"
#import "ATMGetAtmResponse.h"


@interface ATMApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation ATMApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        ATMConfiguration *config = [ATMConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[ATMApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(ATMApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(ATMApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static ATMApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[ATMApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [ATMApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Returns information about atms
/// 
///  @param x longitude coordinate of the ATM
///
///  @param y latitude coordinate of the ATM
///
///  @param distance distance within which to seek ATMs from the coordinates specified
///
///  @param maxResult maximum number of results to include in the results
///
///  @returns ATMGetAtmsResponse*
///
-(NSNumber*) getATMsWithCompletionBlock: (NSNumber*) x
         y: (NSNumber*) y
         distance: (NSNumber*) distance
         maxResult: (NSNumber*) maxResult
        
        completionHandler: (void (^)(ATMGetAtmsResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'x' is set
    if (x == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `x` when calling `getATMs`"];
    }
    
    // verify the required parameter 'y' is set
    if (y == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `y` when calling `getATMs`"];
    }
    
    // verify the required parameter 'distance' is set
    if (distance == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `distance` when calling `getATMs`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bank/v1/atms"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(x != nil) {
        
        queryParams[@"x"] = x;
    }
    if(y != nil) {
        
        queryParams[@"y"] = y;
    }
    if(distance != nil) {
        
        queryParams[@"distance"] = distance;
    }
    if(maxResult != nil) {
        
        queryParams[@"maxResult"] = maxResult;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [ATMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [ATMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"ATMGetAtmsResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((ATMGetAtmsResponse*)data, error);
              }
          ];
}

///
/// Returns information about a specific ATM
/// 
///  @param atmId Unique Identifier for the ATM
///
///  @returns ATMGetAtmResponse*
///
-(NSNumber*) getATMWithCompletionBlock: (NSString*) atmId
        
        completionHandler: (void (^)(ATMGetAtmResponse* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'atmId' is set
    if (atmId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `atmId` when calling `getATM`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bank/v1/atms/{atmId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (atmId != nil) {
        pathParams[@"atmId"] = atmId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [ATMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [ATMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"ATMGetAtmResponse*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((ATMGetAtmResponse*)data, error);
              }
          ];
}



@end
