//
//  RequestMoneyViewController.swift
//  TeenWallet
//
//  Created by Perjan Duro on 07/11/15.
//
//

import UIKit

// we will use this view controller to request or send money
class RequestMoneyViewController: UIViewController, UITextFieldDelegate, PDAlertViewControllerDelegate {

    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    let nf = NSNumberFormatter()
    var recipientName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        amountTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendMoney(sender: AnyObject) {
        KVNProgress.show()
        amountTextField.resignFirstResponder()
        let paymentManager = PDPaymentManager()
        
        guard let amountString = amountTextField.text else {return}
        guard let amount = nf.numberFromString(amountString) else {return}
        
        paymentManager.recipientFromAccount("1") { (success, recipient) -> Void in
            if success {
                paymentManager.makePayment(amount, currency: "EUR", recipient: recipient, sender: paymentManager.demoSender(), completionBlock: { (success, draftTransactionId) -> Void in
                    if success {
                        paymentManager.verifyPaymentSMS("000", transactionId:draftTransactionId!, completionBlock: { (success) -> Void in
                            if success {
                                KVNProgress.dismiss()
                                self.showSuccessAlert()
                            }
                        })
                    }
                })
            }
        }
        
    }
    
    func showSuccessAlert() {
        let alert = PDAlertViewController(longAlert: nil, subtitle: nil, image: nil)
        alert.delegate = self
        self.navigationController!.presentPopinController(alert, animated: true, completion: { () -> Void in
        })
    }
    
    @IBAction func updateButton(sender:UITextField) {
        let currency = nf.numberFromString(sender.text!)
        let title = "Send \(nf.stringFromNumber(currency!)) to John"
        sendButton.setTitle(title, forState: .Normal)
    }
    
    func alertViewModal(alertView: PDAlertViewController, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            alertView.setPopinTransitionDirection(BKTPopinTransitionDirection.Top)
//            self.navigationController?.modalTransitionStyle = .CrossDissolve
            self.navigationController?.dismissCurrentPopinControllerAnimated(true, completion: { () -> Void in
            })
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) { () -> Void in
                self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
                NSNotificationCenter.defaultCenter().postNotificationName("MoneyWasSent", object: self.recipientName)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
